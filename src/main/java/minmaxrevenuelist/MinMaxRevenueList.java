package minmaxrevenuelist;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MinMaxRevenueList {
    public int max(List<Integer> array) {
        int max = 0;

        for (Integer integer : array) {
            if (integer > max) {
                max = integer;
            }
        }
        return max;
    }
    public int min(List<Integer> array) {
        int min = array.get(0);

        for (Integer integer : array) {
            if (integer < min) {
                min = integer;
            }
        }
        return min;
    }
    public static void main(String[] args) {

        System.out.println("Введіть прибуток: ");
        Scanner s = new Scanner(System.in);

        List<Integer> revenue = new ArrayList<>(12);

        while (s.hasNextInt()) {
            int i = s.nextInt();
            revenue.add(i);

            if (revenue.size() >= 12) break;
        }

        System.out.println("Прибуток за 12 місяців: " + revenue);


        MinMaxRevenueList m = new MinMaxRevenueList();
        System.out.println("Максимальний прибуток: " + m.max(revenue));
        System.out.println("Мінімальний прибуток: " + m.min(revenue));
    }
}
