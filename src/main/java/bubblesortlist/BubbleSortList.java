package bubblesortlist;


import java.util.ArrayList;

public class BubbleSortList {
    static void bubbleSort(ArrayList<Integer> sort) {
        int n = sort.size();
        for (int i = 0; i < n - 1; i++) {
            boolean swapped = false;
            for (int j = 0; j < n - i - 1; j++) {
                if (sort.get(j) > sort.get(j + 1)) {
                    int temp = sort.get(j);
                    sort.set(j, sort.get(j + 1));
                    sort.set(j + 1, temp);
                    swapped = true;
                }
            }
            if (!swapped)
                break;
        }
    }
    public static void main(String[] args) {
        ArrayList<Integer> bubble = new ArrayList<>();
        bubble.add(64);
        bubble.add(-3);
        bubble.add(-18);
        bubble.add(0);
        bubble.add(22);
        bubble.add(11);
        bubble.add(90);
        System.out.println("Початкова колекція:\n" + bubble);
        bubbleSort(bubble);
        System.out.println("Колекція, відсортована методом бульбашок:\n" + bubble);
    }
}
