package shufflelist;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ShuffleList {
    public static void main(String[] args) {

        List<String> list = Arrays.asList("A", "B", "C", "D", "E", "F", "G", "H", "1", "2", "3", "4", "5", "6", "7", "8");

        System.out.println("Початкова колекція:\n" + list);

        Collections.shuffle(list);
        System.out.println("Перемішана колекція 1:\n" + list);

        Collections.shuffle(list);
        System.out.println("Перемішана колекція 2:\n" + list);
    }
}
