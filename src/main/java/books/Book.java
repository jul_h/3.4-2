package books;

import com.github.javafaker.Faker;

public class Book {
    private final String author;
    private final String title;
    private final String publisher;
    private final String isbn;
    private static final Faker faker = new Faker();
    private boolean isOrdered;

    public Book(String author, String title, String publisher, String isbn) {

        this.author = author;
        this.title = title;
        this.publisher = publisher;
        this.isbn = faker.idNumber().valid();
        isOrdered = false;
    }

    public Book() {
        this.author = faker.book().author();
        this.title = faker.book().title();
        this.publisher = faker.book().publisher();
        this.isbn = faker.idNumber().valid();
        isOrdered = false;
    }

    public void makeBookAvailable() {
        this.isOrdered = false;
    }

    public void makeBookOrdered() {
        this.isOrdered = true;
    }


    @Override
    public String toString() {
        return "Author: " + author + ", title: " + title + ", publisher: " + publisher + ", ISBN: " + isbn;
    }


}


