package books;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Library {
    private List<Book> availableBooks;
    private List<Reader> readers;
    private List<Employee> employees;


    public Library(int booksQuantity) {
        availableBooks = new ArrayList<>(booksQuantity);
        readers = new LinkedList<>();
        employees = new LinkedList<>();

        for (int i = 0; i < booksQuantity; i++) {
            availableBooks.add(new Book());
        }
    }

    public List<Book> getAvailableBooks() {
        return availableBooks;
    }

    public List<Reader> getReaders() {
        return readers;
    }

    public void registerNewReader(String fullName, int age, String sex, String phoneNumber, String registrationDate) {
        Reader newReader = new Reader(fullName, age, sex, phoneNumber, registrationDate);
        readers.add(newReader);
        System.out.println("\nДодано читача:\n" + newReader);
    }

    public void addBook(String author, String title, String publisher, String isbn) {
        availableBooks.add(new Book(author, title, publisher, isbn));
    }

    public void registerNewEmployee(String fullName, int age, String sex, String phoneNumber, String post, String salary) {
        Employee newEmployee = new Employee(fullName, age, sex, phoneNumber, post, salary);
        employees.add(newEmployee);
//        System.out.println("Додано працівника:\n" + newEmployee);

        System.out.println("\nДодано працівника:\n" + employees.get(employees.size() - 1));
    }

    public void reserveBook(Reader registeredReader, Book addedBook) {

        addedBook.makeBookOrdered();

        registeredReader.getOrderedBooks().add(addedBook);

        availableBooks.remove(addedBook);

        System.out.println("\nЧитач " + registeredReader + " забронював книгу " + addedBook);
    }

    public void averageReadersAge() {
        float totalAge = 0;

        for (int i = 0; i < readers.size(); i++) {
            totalAge = totalAge + readers.get(i).getAge();
        }

        float average = totalAge / readers.size();

        System.out.println("\nСередній вік читачів: " + average);
    }


    @Override
    public String toString() {
        String result = "";
        for (int n = 0; n < availableBooks.size(); n++) {
            result = result + availableBooks.get(n) + "\n";
        }
        return result;
    }
}
