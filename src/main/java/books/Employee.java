package books;

public class Employee extends Person {
    protected String post;
    protected String salary;


    public Employee(String fullName, int age, String sex, String phoneNumber, String post, String salary) {
        this.fullName = fullName;
        this.age = age;
        this.sex = sex;
        this.phoneNumber = phoneNumber;
        this.post = post;
        this.salary = salary;
    }
    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }
    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Full name: " + fullName + "; age: " + age + "; sex: " + sex + "; phone number: " + phoneNumber + "; post: " + post + "; salary: " + salary;
    }
}
