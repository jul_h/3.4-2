package books;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.TreeSet;

public class Reader extends Person {
    protected String registrationDate;
    protected LinkedList<Book> orderedBooks;


    public Reader(String fullName, int age, String sex, String phoneNumber, String registrationDate) {
        this.fullName = fullName;
        this.age = age;
        this.sex = sex;
        this.phoneNumber = phoneNumber;
        this.registrationDate = registrationDate;
        this.orderedBooks = new LinkedList<>();
    }

    public String getRegistrationDate() {
        return registrationDate;
    }

    public LinkedList<Book> getOrderedBooks() {
        return orderedBooks;
    }

    public void takeBooks(LinkedList<Book> orderedBooks) {
        this.orderedBooks.addAll(orderedBooks);
    }
    public void getBackBooks(Integer[] bookNumber) {
        TreeSet<Integer> bookNumberSet = new TreeSet<>(Arrays.asList(bookNumber));

        for(int i = 0; i < bookNumberSet.size(); i++) {
            if (bookNumberSet.last() > bookNumberSet.size()) {
                System.out.println("Sorry! There is no such quantity of books");
            }
            orderedBooks.get(bookNumberSet.last()).makeBookAvailable();
            bookNumberSet.remove(bookNumberSet.pollLast());
        }
    }


    @Override
    public String toString() {
    return "Full name: " + fullName + "; age: " + age + "; sex: " + sex + "; phone number: " + phoneNumber + "; registration date: " + registrationDate;
    }
}


