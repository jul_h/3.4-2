package books;

public class Main {

    public static void main(String[] args) {
        Library library = new Library(50);
        System.out.println("Список наявних книг:\n" + library);

        library.registerNewReader("Cowabunga L.", 21, "f", "517-815-5856", "21.01.2012");
        library.registerNewReader("Emerson A.", 27, "m", "409-861-0880", "12.04.2019");
        library.registerNewReader("Roe J.", 23, "f", "314-407-5047", "10.09.2021");
        library.registerNewReader("Estes C.", 51, "m", "660-616-4097", "21.01.2000");
        library.registerNewReader("Kai I.", 44, "f", "563-225-5064", "02.11.2006");
        library.registerNewReader("Egawa G.", 29, "m", "240-343-8327", "30.05.2013");
        library.registerNewReader("Winters K.", 37, "f", "916-441-9560", "16.07.2010");


        library.averageReadersAge();

        library.addBook("Vernia Satterfield", "Nine Coaches Waiting", "Elsevier", "164-61-9764");
        library.addBook("Dorcas Hettinger", "Postern of Fate", "Leaf Books", "460-07-0223");
        library.addBook("Miss Ivonne Crona", "The Millstone", "New English Library", "409-99-2001");
        library.addBook("Hector Dibbert Sr.", "Dance Dance Dance", "Schocken Books", "063-64-0625");
        library.addBook("Eliseo Huels", "Many Waters", "Blackie and Son Limited", "416-42-7632");
        library.addBook("Miss Claudio Zulauf", "Behold the Man", "Open Court Publishing Company", "106-63-3560");
        library.addBook("Dr. Emmanuel Cormier", "The Cricket on the Hearth", "Pavilion Books", "044-82-0725");
        library.addBook("Johnathon Dicki", "The Daffodil Sky", "Carlton Publishing Group", "422-93-7681");
        library.addBook("Jacinda Pouros", "Death Be Not Proud", "Harvard University Press", "414-05-7454");
        library.addBook("Malik Pouros", "Blithe Spirit", "Kensington Books", "079-75-8409");
        library.addBook("Jolyn Haley", "Clouds of Witness", "Open Court Publishing Company", "232-87-9551");
        library.addBook("Alethea Cartwright", "Infinite Jest", "Hay House", "319-48-0596");
        library.addBook("Isis Wehner", "Vanity Fair", "Bantam Books", "396-08-9950");
        library.addBook("Trinidad Pollich I", "For Whom the Bell Tolls", "Bella Books", "704-45-5260");
        library.addBook("Dwight Hermiston DDS", "The Far-Distant Oxus", "Random House", "409-09-4207");
        library.addBook("Evelyn Herzog", "Blood's a Rover", "McGraw Hill Financial", "698-07-1959");
        library.addBook("Wilmer Cole", "The Daffodil Sky", "Earthscan", "721-70-1540");
        library.addBook("Arla Roob MD", "Fear and Trembling", "Earthscan", "610-31-9022");
        library.addBook("Miss Leo Beier", "A Swiftly Tilting Planet", "New Village Press", "271-70-9701");
        library.addBook("Ms. Shakia Lind", "The Little Foxes", "Breslov Research Institute", "492-26-3779");
        library.addBook("Kayla Effertz", "The Wind's Twelve Quarters", "Koren Publishers Jerusalem", "854-71-8875");
        library.addBook("Leopoldo Cartwright", "Stranger in a Strange Land", "Bloodaxe Books", "429-24-2926");
        library.addBook("Sylvia Greenholt", "The Violent Bear It Away", "Dalkey Archive Press", "137-52-9678");
        library.addBook("Mrs. Holley Simonis", "An Acceptable Time", "Harcourt Assessment", "398-79-7287");
        library.addBook("Vicente Gutmann", "Mother Night", "Bowes & Bowes", "834-80-9353");

        library.registerNewEmployee("Olle Numminen", 29, "m", "046 219 2258", "librarian", "$55,248");
        library.registerNewEmployee("Kyra Macleod", 45, "f", "323-743-1468", "senior librarian", "$75,500");
        library.registerNewEmployee("Dalia Alvarez Beltrán", 35, "m", "870-743-0552", "librarian", "$60,345");

        Reader readerForReserving = library.getReaders().get(0);
        Book bookForReserving = library.getAvailableBooks().get(5);

        library.reserveBook(readerForReserving, bookForReserving);
    }
}
